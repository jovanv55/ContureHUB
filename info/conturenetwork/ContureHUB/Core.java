package info.conturenetwork.ContureHUB;

import org.bukkit.Bukkit;
import org.bukkit.World;

import info.conturenetwork.ContureHUB.commands.ContureCommand;
import info.conturenetwork.ContureHUB.commands.PlayCommand;
import info.conturenetwork.ContureHUB.commands.ReloadCommand;
import info.conturenetwork.ContureHUB.commands.SpawnCommand;
import info.conturenetwork.ContureHUB.handlers.HubHandler;
import info.conturenetwork.ContureHUB.handlers.ServersHandler;
import info.conturenetwork.ContureHUB.utilities.Vault;
import info.conturenetwork.ContureHUB.utilities.scoreboard.API.Frame;
import info.conturenetwork.ContureHUB.utilities.tab.Layout;

public class Core extends org.bukkit.plugin.java.JavaPlugin
{
  private static Core instance;
  private ServersHandler serversHandler;
  
  public Core() {}
  
  public void onEnable()
  {
    instance = this;
    try {
      enable();
    } catch(Error err) {
    	Bukkit.getConsoleSender().sendMessage("§7==============================");
        Bukkit.getConsoleSender().sendMessage("§3Conture Hub Core §edobio error dok se palio idi kill se.");
        Bukkit.getConsoleSender().sendMessage("§3E da i send mi" + err);
        Bukkit.getConsoleSender().sendMessage("§7==============================");	
    } finally {
        Bukkit.getConsoleSender().sendMessage("§7==============================");
        Bukkit.getConsoleSender().sendMessage("§3Conture Hub Core §eje upaljen.");
        Bukkit.getConsoleSender().sendMessage("§3Verzija: §e" + getDescription().getVersion());
        Bukkit.getConsoleSender().sendMessage("§3Autor: §eAnknown");
        Bukkit.getConsoleSender().sendMessage("§7==============================");	
    }
  }
  
  public void onDisable() 
  {
    instance = null;
    Bukkit.getConsoleSender().sendMessage("§7==============================");
    Bukkit.getConsoleSender().sendMessage("§3Conture Hub Core §eje ugasen.");
    Bukkit.getConsoleSender().sendMessage("§3Verzija: §e" + getDescription().getVersion());
    Bukkit.getConsoleSender().sendMessage("§3Autor: §eAnknown");
    Bukkit.getConsoleSender().sendMessage("§7==============================");
  }
  
  public void enable() {
    handlers();
    commands();
    Vault.hook();
    scoreboard();
    settings();
    config();
    serversHandler = new ServersHandler(this);
    tab();
  }
  public void tab() {
	new Layout(this);
  }
  public void handlers() {
    new HubHandler(this);
    new ServersHandler(this);
    new info.conturenetwork.ContureHUB.queue.QueueManager();
    Bukkit.getConsoleSender().sendMessage("§7[§3ContureHub§7] §eSvi handleri uspesno registrovani.");
  }
  
  private void config() {
    getConfig().options().copyDefaults(true);
    saveConfig();
  }
  
  public void scoreboard() {
    new Frame(this, new info.conturenetwork.ContureHUB.utilities.scoreboard.Scoreboard(this));
  }
  
  public void commands() 
  	{
	try 
	{
    getCommand("play").setExecutor(new PlayCommand());
    getCommand("setspawn").setExecutor(new SpawnCommand());
    getCommand("spawn").setExecutor(new SpawnCommand());
    getCommand("reload").setExecutor(new ReloadCommand());
    getCommand("conturecore").setExecutor(new ContureCommand());
	} catch(Error error)
	{
    Bukkit.getConsoleSender().sendMessage("§7[§3ContureHub§7] §eBlame yorself xd for error while loading commands.");
	} finally {
	    Bukkit.getConsoleSender().sendMessage("§7[§3ContureHub§7] §eSve komande uspesno registrovane.");
	}
  }
  
  public void settings() {
    World world = (World)getServer().getWorlds().get(0);
    world.setAmbientSpawnLimit(0);
    world.setAnimalSpawnLimit(0);
    world.setDifficulty(org.bukkit.Difficulty.PEACEFUL);
    world.setWeatherDuration(0);
    world.setKeepSpawnInMemory(false);
    world.setMonsterSpawnLimit(0);
    world.setPVP(false);
    world.setStorm(false);
    world.setTime(0L);
  }
  
  public ServersHandler getServersHandler() {
    return serversHandler;
  }
  
  public static Core getPlugin() {
    return instance;
  }
  
  public static Core getInstance() {
    return instance;
  }
}
