package info.conturenetwork.ContureHUB.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.xephi.authme.libs.javax.inject.Inject;
import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.queue.QueueManager;
public class ContureCommand implements CommandExecutor
{
	  public ContureCommand() {}
	  public boolean onCommand(CommandSender sender,Command cmd, String label, String[] args)
	  {
		Player player = (Player)sender;
	  if ((args.length == 1) && (args[0].equalsIgnoreCase("reload")))
	  {
		    if (player.hasPermission("conturecore.reload") || player.hasPermission("conturecore.admin")) {
		          Core.getInstance().reloadConfig();
		          Core.getInstance().saveConfig();
		          player.sendMessage("§a§lConfig succesfully reloaded.");
		        } else {
		        	player.sendMessage("§eYou do not have permission.");
		        }
		     return true;
	  }
	  if ((args.length == 1) && (args[0].equalsIgnoreCase("play")))
	  {
		    if (!(sender instanceof Player)) {
		        sender.sendMessage("§cThis is for player use only!");
		        return false;
		      }
		      if (args.length == 1) {
		        player.sendMessage("§bUse /conturecore play <server>.");
		        return false;
		      }
		      String server = args[0].toUpperCase();
		      if (server.equalsIgnoreCase("UHC")) {
		        QueueManager.getInstance().getQueue("UHC").addEntry(player);
		      }
		      else if (server.equalsIgnoreCase("UHCMeetup")) {
		        QueueManager.getInstance().getQueue("UHCMeetup").addEntry(player);
		      } else
		        player.sendMessage("§bWe couldn't find the queue for server §f'" + args[0] + "'");
		     return true;
	  }
	  if (args.length == 1 && (args[0].equalsIgnoreCase("spawn")))
      {
          Player p = (Player)sender;
          p.teleport(new Location(org.bukkit.Bukkit.getWorld(Core.getInstance().getConfig().getString("Spawn.world")), Core.getInstance().getConfig().getDouble("Spawn.x"), Core.getInstance().getConfig().getDouble("Spawn.y"), Core.getInstance().getConfig().getDouble("Spawn.z")));
          p.getLocation().setYaw(Core.getInstance().getConfig().getInt("Spawn.yaw"));
          p.getLocation().setPitch(Core.getInstance().getConfig().getInt("Spawn.pitch"));
      }
	  if (args.length == 1 && (args[0].equalsIgnoreCase("setspawn")))
	  {
	        if (!player.hasPermission("conturecore.setspawn") || player.hasPermission("conturecore.admin")) {
	          player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&l» &7You don't have permission!"));
	          return true;
	        }
	        double x = player.getLocation().getX();
	        double y = player.getLocation().getY();
	        double z = player.getLocation().getZ();
	        Core.getInstance().getConfig().set("Spawn.world", player.getLocation().getWorld().getName());
	        Core.getInstance().getConfig().set("Spawn.x", Double.valueOf(player.getLocation().getX()));
	        Core.getInstance().getConfig().set("Spawn.y", Double.valueOf(player.getLocation().getY()));
	        Core.getInstance().getConfig().set("Spawn.z", Double.valueOf(player.getLocation().getZ()));
	        Core.getInstance().getConfig().set("Spawn.yaw", Float.valueOf(player.getLocation().getYaw()));
	        Core.getInstance().getConfig().set("Spawn.pitch", Float.valueOf(player.getLocation().getPitch()));
	        Core.getInstance().saveConfig();
	        player.sendMessage(info.conturenetwork.ContureHUB.utilities.Utils.colors("&a&lYou have created a Spawn to your current location &f(" + x + y + z + ")"));
	        return true;
	  }
	  if ((args.length == 1) && (args[0].equalsIgnoreCase("admin")))
	  {
		     if (player.hasPermission("conturecore.admin")) 
		     {
		  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
		  	 player.sendMessage("§8» §7/ContureCore reload: §6to reload config");
		  	 player.sendMessage("§8» §7/ContureCore setspawn: §6to set spawn");
		  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
		     return true;
		     } else {
			  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
			  	 player.sendMessage("§8» §7/ContureCore play: §6to play on servers");
			  	 player.sendMessage("§8» §7/ContureCore spawn: §6to go to spawn");
			  	 player.sendMessage("§8» §7/ContureCore §4admin§7: §6Admin commands");
			  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
			     return true;
		     }
	  } else {
		  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
		  	 player.sendMessage("§8» §7/ContureCore play: §6to play on servers");
		  	 player.sendMessage("§8» §7/ContureCore spawn: §6to go to spawn");
		  	 player.sendMessage("§8» §7/ContureCore §4admin§7: §6Admin commands");
		  	 player.sendMessage("§7§l[]----------<§6§l ContureCore §7§l>----------[]");
		     return true;
	   }
	  }
}