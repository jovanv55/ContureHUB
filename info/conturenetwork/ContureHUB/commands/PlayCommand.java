package info.conturenetwork.ContureHUB.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import info.conturenetwork.ContureHUB.queue.QueueManager;

public class PlayCommand implements org.bukkit.command.CommandExecutor
{
  public PlayCommand() {}
  
  public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args)
  {
    if (!(sender instanceof Player)) {
      sender.sendMessage("§cThis is for player use only!");
      return false;
    }
    Player player = (Player)sender;
    if (args.length == 0) {
      player.sendMessage("§bUse /play <server>.");
      return false;
    }
    String server = args[0].toUpperCase();
    if (server.equalsIgnoreCase("UHC")) {
      QueueManager.getInstance().getQueue("UHC").addEntry(player);
    } else if (server.equalsIgnoreCase("UHCMeetup")) {
      QueueManager.getInstance().getQueue("UHCMeetup").addEntry(player);
      return false;
    }
	return false;
 }
}
