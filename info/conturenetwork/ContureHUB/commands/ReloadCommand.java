package info.conturenetwork.ContureHUB.commands;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import info.conturenetwork.ContureHUB.Core;

public class ReloadCommand implements org.bukkit.command.CommandExecutor
{
  public ReloadCommand() {}
  
  public boolean onCommand(org.bukkit.command.CommandSender sender, Command cmd, String label, String[] args)
  {
    Player player = (Player)sender;
    if (player.hasPermission("conturecore.reload")) {
        if (args[0].equalsIgnoreCase("reload")) {
          Core.getInstance().reloadConfig();
          Core.getInstance().saveConfig();
          player.sendMessage("§a§lConfig succesfully reloaded.");
        } else { player.sendMessage("§eAvailable command is /Core reload.");
        }
    } else player.sendMessage("§eYou do not have permission.");
    return true;
  }
}
