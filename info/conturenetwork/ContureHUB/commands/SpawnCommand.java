package info.conturenetwork.ContureHUB.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import info.conturenetwork.ContureHUB.Core;

public class SpawnCommand implements org.bukkit.command.CommandExecutor
{
  public SpawnCommand() {}
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (cmd.getName().equalsIgnoreCase("setspawn")) {
      if ((!sender.hasPermission("uhc.setspawn")) && (!sender.hasPermission("uhc.*")) && (!sender.hasPermission("*"))) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&l» &7You don't have permission!"));
        return true;
      }
      if (!(sender instanceof Player)) {
        sender.sendMessage("Only players may use this command.");
        return true;
      }
      Player p = (Player)sender;
      if (!p.hasPermission("uhc.setspawn")) {
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&l» &7You don't have permission!"));
        return true;
      }
      double x = p.getLocation().getX();
      double y = p.getLocation().getY();
      double z = p.getLocation().getZ();
      Core.getInstance().getConfig().set("Spawn.world", p.getLocation().getWorld().getName());
      Core.getInstance().getConfig().set("Spawn.x", Double.valueOf(p.getLocation().getX()));
      Core.getInstance().getConfig().set("Spawn.y", Double.valueOf(p.getLocation().getY()));
      Core.getInstance().getConfig().set("Spawn.z", Double.valueOf(p.getLocation().getZ()));
      Core.getInstance().getConfig().set("Spawn.yaw", Float.valueOf(p.getLocation().getYaw()));
      Core.getInstance().getConfig().set("Spawn.pitch", Float.valueOf(p.getLocation().getPitch()));
      Core.getInstance().saveConfig();
      p.sendMessage(info.conturenetwork.ContureHUB.utilities.Utils.colors("&a&lYou have created a Spawn to your current location &f(" + x + y + z + ")"));
      return true;
    }
    if (cmd.getName().equalsIgnoreCase("spawn")) {
      Player p = (Player)sender;
      p.teleport(new Location(org.bukkit.Bukkit.getWorld(Core.getInstance().getConfig().getString("Spawn.world")), Core.getInstance().getConfig().getDouble("Spawn.x"), Core.getInstance().getConfig().getDouble("Spawn.y"), Core.getInstance().getConfig().getDouble("Spawn.z")));
      p.getLocation().setYaw(Core.getInstance().getConfig().getInt("Spawn.yaw"));
      p.getLocation().setPitch(Core.getInstance().getConfig().getInt("Spawn.pitch"));
    }
    return false;
  }
}
