package info.conturenetwork.ContureHUB.handlers;

import java.util.ArrayList;
import java.util.Iterator;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.Vector;

import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.utilities.ItemBuilder;
import info.conturenetwork.ContureHUB.utilities.Utils;

public class HubHandler implements Listener
{
  public HubHandler(Core plugin)
  {
    Core.getInstance().getServer().getPluginManager().registerEvents(this, Core.getInstance());
  }
  
  private ItemStack helmet = new ItemBuilder(Material.LEATHER_HELMET).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setLeatherArmorColor(Color.PURPLE).setName(Utils.colors("&5&lSTAFF")).toItemStack();
  private ItemStack chestplate = new ItemBuilder(Material.LEATHER_CHESTPLATE).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setLeatherArmorColor(Color.PURPLE).setName(Utils.colors("&5&lSTAFF")).toItemStack();
  private ItemStack leggings = new ItemBuilder(Material.LEATHER_LEGGINGS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setLeatherArmorColor(Color.PURPLE).setName(Utils.colors("&5&lSTAFF")).toItemStack();
  private ItemStack boots = new ItemBuilder(Material.LEATHER_BOOTS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setLeatherArmorColor(Color.PURPLE).setName(Utils.colors("&5&lSTAFF")).toItemStack();
  

  public ItemStack selector(Material material, String name)
  {
    ItemStack item = new ItemStack(material);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName(name);
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    String sLore = Core.getInstance().getConfig().getString("JoinItems.Selector.Lore").replaceAll("&", "§");
    lore.add(sLore);
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    
    return item;
  };
  
  @EventHandler
  public void onJoin(PlayerJoinEvent e) {
    final Player player = e.getPlayer();
    e.setJoinMessage(null);
    for (int i = 0; i < 100; i++) {
      player.sendMessage("");
    }
    for (String msg : Core.getInstance().getConfig().getStringList("JoinMessage")) {
      player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg).replaceAll("%player%", player.getName()));
    }
    player.getInventory().clear();
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.getInstance(), new Runnable() {
      public void run() {
        int s = Core.getInstance().getConfig().getInt("JoinItems.Selector.Slot");
        String sName = Core.getInstance().getConfig().getString("JoinItems.Selector.Name").replaceAll("&", "§");
        player.getInventory().clear();
        player.getInventory().setItem(s, selector(Material.COMPASS, sName));
      }
    }, 2L);
    player.getInventory().setHeldItemSlot(4);
    player.setFoodLevel(20);
    if (player.hasPermission("Core.staff")) {
      PlayerInventory playerInventory = player.getInventory();
      playerInventory.setHelmet(helmet);
      playerInventory.setChestplate(chestplate);
      playerInventory.setLeggings(leggings);
      playerInventory.setBoots(boots);
    }
    if (player.getWalkSpeed() != 0.5F) {
      player.setWalkSpeed(0.5F);
    }
    player.teleport(new Location(Bukkit.getWorld(Core.getInstance().getConfig().getString("Spawn.world")), 
    Core.getInstance().getConfig().getDouble("Spawn.x"), 
    Core.getInstance().getConfig().getDouble("Spawn.y"), 
    Core.getInstance().getConfig().getDouble("Spawn.z")));
    player.getLocation().setYaw(Core.getInstance().getConfig().getInt("Spawn.yaw"));
    player.getLocation().setPitch(Core.getInstance().getConfig().getInt("Spawn.pitch"));
  }
  
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event) {
    Player player = event.getPlayer();
    ItemStack item = event.getItem();
    //Action action = event.getAction();
    if ((item == null) || (item.getType().equals(Material.AIR))) {
      return;
    }
    if (!item.hasItemMeta()) {
      return;
    }
    if (!item.getItemMeta().hasDisplayName()) {
      return;
    }
    if ((item.getType() == Material.COMPASS) && (item.getItemMeta().getDisplayName().equals(Core.getInstance().getConfig().getString("JoinItems.Selector.Name").replaceAll("&", "§")))) {
      player.openInventory(servers(player));
    }
  }
  public Inventory servers(Player player)
  {
    Inventory inv = Bukkit.createInventory(null, 27, "§d§lSelect Server");
    inv.setItem(0, filler());
    inv.setItem(1, filler());
    inv.setItem(2, filler());
    inv.setItem(3, filler());
    inv.setItem(4, filler());
    inv.setItem(5, filler());
    inv.setItem(6, filler());
    inv.setItem(7, filler());
    inv.setItem(8, filler());
    inv.setItem(9, filler());
    inv.setItem(10, filler());
    inv.setItem(11, filler());
    inv.setItem(13, filler());
    inv.setItem(16, filler());
    inv.setItem(17, filler());
    inv.setItem(18, filler());
    inv.setItem(19, filler());
    inv.setItem(20, filler());
    inv.setItem(21, filler());
    inv.setItem(22, filler());
    inv.setItem(23, filler());
    inv.setItem(24, filler());
    inv.setItem(25, filler());
    inv.setItem(26, filler());
    inv.setItem(15, filler());
    inv.setItem(12, uhc());
    inv.setItem(14, skypvp());
    return inv;
  }
  public Inventory mservers(Player player)
  {
    Inventory inv = Bukkit.createInventory(null, 27, "§d§lServers");
    inv.setItem(0, filler());
    inv.setItem(1, filler());
    inv.setItem(2, filler());
    inv.setItem(3, filler());
    inv.setItem(4, filler());
    inv.setItem(5, filler());
    inv.setItem(6, filler());
    inv.setItem(7, filler());
    inv.setItem(8, filler());
    inv.setItem(9, filler());
    inv.setItem(10, filler());
    inv.setItem(12, filler());
    inv.setItem(14, filler());
    inv.setItem(16, filler());
    inv.setItem(17, filler());
    inv.setItem(18, filler());
    inv.setItem(19, filler());
    inv.setItem(20, filler());
    inv.setItem(21, filler());
    inv.setItem(22, filler());
    inv.setItem(23, filler());
    inv.setItem(24, filler());
    inv.setItem(25, filler());
    inv.setItem(26, filler());
    inv.setItem(14, filler());
    inv.setItem(11, m1());
    inv.setItem(13, m2());
    inv.setItem(15, m3());
    return inv;
  }
  public ItemStack filler() 
  {
	    ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
	    ItemMeta itemmeta = glass.getItemMeta();
	    itemmeta.setDisplayName("§d§lConture Network");
	    glass.setItemMeta(itemmeta);
	    return glass;
  }
  public ItemStack skypvp()
  {
	String status;
	if (Core.getInstance().getServersHandler().isonlineminigames()) status = "§aOnline";
	else status = "§4Offline";
    ItemStack item = new ItemStack(Material.GOLDEN_APPLE, 1);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName("§d§lUHCMeetup");
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    lore.add("§8§m-------------------------------");
    lore.add("§7* §fPlay with your friends, Lore and try to win.");
    lore.add("§7* §fOpen the folds by map and fit as much as possible.");
    lore.add("§7* §fAnd when it starts pvp then it is shown.");
    lore.add("");
    lore.add("§7* §d§lClick this to open UHCMeetup.");
    lore.add("§8§m-------------------------------");
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    return item;
  }
  public ItemStack m1()
  {
	String status;
	if (Core.getInstance().getServersHandler().isonlineminigames()) status = "§aOnline";
	else status = "§4Offline";
    ItemStack item = new ItemStack(Material.WOOL, 1, (short) 4);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName("§d§lUHCMeetup 1");
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    lore.add("§8§m-------------------------------");
    lore.add("§7* §fPlay with your friends, Lore and try to win.");
    lore.add("§7* §fOpen the folds by map and fit as much as possible.");
    lore.add("§7* §fAnd when it starts pvp then it is shown.");
    lore.add("");
    lore.add("§7* §d§lClick this to open UHCMeetup.");
    lore.add("§8§m-------------------------------");
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    return item;
  }
  public ItemStack m2()
  {
	String status;
	if (Core.getInstance().getServersHandler().isonlineminigames()) status = "§aOnline";
	else status = "§4Offline";
    ItemStack item = new ItemStack(Material.WOOL, 1, (short) 4);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName("§d§lUHCMeetup 2");
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    lore.add("§8§m-------------------------------");
    lore.add("§7* §fPlay with your friends, Lore and try to win.");
    lore.add("§7* §fOpen the folds by map and fit as much as possible.");
    lore.add("§7* §fAnd when it starts pvp then it is shown.");
    lore.add("");
    lore.add("§7* §d§lClick this to open UHCMeetup.");
    lore.add("§8§m-------------------------------");
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    return item;
  }
  public ItemStack m3()
  {
	String status;
	if (Core.getInstance().getServersHandler().isonlineminigames()) status = "§aOnline";
	else status = "§4Offline";
    ItemStack item = new ItemStack(Material.WOOL, 1, (short) 4);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName("§d§lUHCMeetup 3");
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    lore.add("§8§m-------------------------------");
    lore.add("§7* §fPlay with your friends, Lore and try to win.");
    lore.add("§7* §fOpen the folds by map and fit as much as possible.");
    lore.add("§7* §fAnd when it starts pvp then it is shown.");
    lore.add("");
    lore.add("§7* §d§lClick this to open UHCMeetup.");
    lore.add("§8§m-------------------------------");
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    return item;
  }
  public ItemStack uhc()
  {
	String status;
	if (Core.getInstance().getServersHandler().isonlineUHC() == true) status = "§aOnline";
	else status = "§4Offline";
    ItemStack item = new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1);
    ItemMeta itemmeta = item.getItemMeta();
    itemmeta.setDisplayName("§d§lUHC");
    item.setItemMeta(itemmeta);
    ArrayList<String> lore = new ArrayList();
    lore.add("§7§m-------------------------------");
    lore.add("§7* §fNo natural regen, closing border.");
    lore.add("§7* §fDifferent gamemodes.");
    lore.add("§7* §fLast alive player is winner.");
    lore.add("");
    lore.add("§7* §fStatus: " + status);
    lore.add("§7* §fPlayers: " + Core.getInstance().getServersHandler().getUHCCount());
    lore.add("§7* §d§lClick this to connect to UHC.");
    lore.add("§7§m-------------------------------");
    itemmeta.setLore(lore);
    item.setItemMeta(itemmeta);
    return item;
  }
  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Player player = (Player)event.getWhoClicked();
    ItemStack item = event.getCurrentItem();
    InventoryAction action = event.getAction();
    if (event.getSlotType().equals(InventoryType.SlotType.OUTSIDE)) {
      event.setCancelled(true);
      return;
    }
    if (((item == null) || (item.getType().equals(Material.AIR))) && ((action.equals(InventoryAction.HOTBAR_SWAP)) || 
      (action.equals(InventoryAction.SWAP_WITH_CURSOR)))) {
      event.setCancelled(true);
      return;
    }
    if ((item.hasItemMeta()) && (item.getItemMeta().hasDisplayName())) {
      if (item.getItemMeta().getDisplayName().equals(Core.getInstance().getConfig().getString("JoinItems.Selector.Name").replaceAll("&", "§"))) {
        event.setCancelled(true);
        player.updateInventory();
      }
      if (event.getInventory().getTitle().equals("§d§lServers")) {
    	  event.setCancelled(true);
    	  if (item.getItemMeta().getDisplayName().equals("§d§lUHCMeetup 1")) {
    		  player.performCommand("play UHCMeetup");
    	  }
      }
      if (event.getInventory().getTitle().equals("§d§lSelect Server")) {
        if (item.getItemMeta().getDisplayName().equals("§d§lUHC")) {
          event.setCancelled(true);
          player.closeInventory();
          player.performCommand("play uhc");
        };
        if (item.getItemMeta().getDisplayName().equals("§d§lUHCMeetup")) {
            event.setCancelled(true);
            player.closeInventory();
          //  player.performCommand("play MiniGames");
            player.openInventory(mservers(player));
        }
        if (item.getItemMeta().getDisplayName().equals("§d§lConture Network")) 
        {
      	  event.setCancelled(true);
        };
      };
    }
  }
  
  @EventHandler
  public void blockbreak(BlockBreakEvent event)
  {
    if (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
      event.setCancelled(true);
    }
  }
  
  @EventHandler
  public void blockplace(BlockPlaceEvent event) {
    if (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
      event.setCancelled(true);
    }
  }
  
  @EventHandler(priority=EventPriority.MONITOR)
  public void onPrime(ExplosionPrimeEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler(priority=EventPriority.MONITOR)
  public void onWeatherChange(WeatherChangeEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler
  public void Explotion(EntityDamageEvent event) {
    if ((event.getCause().equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)) || (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION)))
      event.setCancelled(true);
  }
  
  @EventHandler
  public void CancelDamage(EntityDamageEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler
  public void PVP(EntityDamageByEntityEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler
  public void food(FoodLevelChangeEvent event) {
    event.setFoodLevel(20);
  }
  
  @EventHandler(priority=EventPriority.HIGHEST)
  public void DropItem(PlayerDropItemEvent event) {
    event.setCancelled(true);
  }
  
  public void onPick(PlayerPickupItemEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onInventoryMoveItemEvent(InventoryMoveItemEvent event) {
    event.setCancelled(true);
  }
  
  @EventHandler
  public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
    Player player = event.getPlayer();
    if (player.getGameMode() == GameMode.CREATIVE) {
      return;
    }
    event.setCancelled(true);
    player.setAllowFlight(false);
    player.setFlying(false);
    player.setVelocity(player.getLocation().getDirection().multiply(1.5D).setY(1));
  }
  
  @EventHandler
  public void onPlayerMove(PlayerMoveEvent event) {
    Player player = event.getPlayer();
    if ((player.getGameMode() != GameMode.CREATIVE) && (player.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() != Material.AIR) && (!player.isFlying())) {
      player.setAllowFlight(true);
    }
  }
  
  @EventHandler
  public void onFallDamage(EntityDamageEvent e) {
    if (((e.getEntity() instanceof Player)) && (e.getCause() == EntityDamageEvent.DamageCause.FALL)) {
      e.setCancelled(true);
    }
  }
  
  @EventHandler
  public void onInvDrag(InventoryDragEvent e) {
    e.setCancelled(true);
  }
  
  @EventHandler
  public void move(PlayerMoveEvent e) {
    final Player f = e.getPlayer();
    if (e.getTo().getY() < 2.0D) {
      Core.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Core.getInstance(), new Runnable()
      {
        public void run() {
          double y = f.getLocation().getY() - 2.0D;
          Location l = new Location(f.getLocation().getWorld(), f.getLocation().getX(), y, f.getLocation().getZ(), f.getLocation().getYaw(), f.getLocation().getPitch());
          f.getWorld().playEffect(l, org.bukkit.Effect.ENDER_SIGNAL, 50, 30);
        }
      }, 10L);
    }
  }
  
  public void location(Player p, Location loc) {
    int locY = loc.getBlockY();
    if (locY >= 200.0D) {
      p.performCommand("spawn");
      p.sendMessage(Utils.colors("&a&lYou are teleported to the spawn."));
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onPreCommand(PlayerCommandPreprocessEvent event)
  {
    Player player = event.getPlayer();
    if (player.hasPermission("crash.bypass")) {
      return;
    }
    if ((event.getMessage().toLowerCase().startsWith("/worldedit:/calc")) || 
      (event.getMessage().toLowerCase().startsWith("/worldedit:/eval")) || 
      (event.getMessage().toLowerCase().startsWith("/worldedit:/solve")) || 
      (event.getMessage().toLowerCase().startsWith("//calc")) || 
      (event.getMessage().toLowerCase().startsWith("//eval")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:?")) || 
      (event.getMessage().toLowerCase().startsWith("/?")) || 
      (event.getMessage().toLowerCase().startsWith("/ver")) || 
      (event.getMessage().toLowerCase().startsWith("/pl")) || 
      (event.getMessage().toLowerCase().startsWith("/me")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:about")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:pl")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:me")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:ver")) || 
      (event.getMessage().toLowerCase().startsWith("/bukkit:help")) || 
      (event.getMessage().toLowerCase().startsWith("//solve"))) {
      event.setCancelled(true);
      player.sendMessage("§cPlease don't do that!".replaceAll("&", "§"));
      Player[] onlinePlayers;
      int length = (onlinePlayers = Bukkit.getOnlinePlayers()).length;
      for (int i = 0; i < length; i++)
      {
        Player online = onlinePlayers[i];
        if (online.hasPermission("Conture.staff")) {
          online.sendMessage("§c<player> §7tried §c<command>".replaceAll("&", "§").replace("<player>", player.getName()).replace("<command>", event.getMessage().toString()));
        }
      }
    }
  }
  @EventHandler
  public void leaveMessage(PlayerQuitEvent e) {
    e.setQuitMessage(null);
    Player p = e.getPlayer();
    p.setLevel(0);
  }
  
  public java.util.List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
    return java.util.Collections.emptyList();
  }
}
