package info.conturenetwork.ContureHUB.handlers;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.utilities.Handler;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

public class ServersHandler extends Handler implements PluginMessageListener
{
  private int globalCount;
  private int uhcCount;
  private int minigamesCount;
  private boolean isOnlineminigames;
  private boolean isOnlineUHC;
  public ServersHandler(Core plugin)
  {
    super(plugin);
    globalCount = 0;
    uhcCount = 0;
    minigamesCount = 0;
    isOnlineminigames = false;
    isOnlineUHC = false;
    checkIfServersAreOnline();
    registerServers();
    Bukkit.getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
    Bukkit.getMessenger().registerIncomingPluginChannel(plugin, "BungeeCord", this);
  }
  private void checkIfServersAreOnline()
  {
	int sec = 60;
	new BukkitRunnable()
    {
	public void run()
	  {
	    try {
	      Socket s = new Socket();
	        s.connect(new InetSocketAddress("127.0.0.1", Core.getInstance().getConfig().getInt("Servers.minigamesport")), 20);
	        s.close();
	        isOnlineminigames = true;
	        } catch (Exception e) {
	        isOnlineminigames = false;
	    }
	    try {
		      Socket s = new Socket();
		        s.connect(new InetSocketAddress("127.0.0.1", Core.getInstance().getConfig().getInt("Servers.uhcport")), 20);
		        s.close();
		        isOnlineUHC = true;
		        } catch (Exception e) {
		        isOnlineUHC = false;
		    }
	  }
	}.runTaskTimer(Core.getInstance(), 20L * sec, 20L * sec);
  }
  private void registerServers()
  {
    new BukkitRunnable()
    {
      public void run()
      {
        sendToBungeeCord("PlayerCount", "ALL");
        sendToBungeeCord("PlayerCount", "UHC");
        sendToBungeeCord("PlayerCount", "minigames");
      }
    }.runTaskTimer(Core.getInstance(), 0L, 40L);
  }
  
  public void onPluginMessageReceived(String channel, Player player, byte[] message) {
    if (channel.equals("BungeeCord")) {
      ByteArrayDataInput input = ByteStreams.newDataInput(message);
      String subchannel = input.readUTF();
      //Bukkit.getConsoleSender().sendMessage(subchannel);
      if (subchannel.equals("PlayerCount")) {
        String serverName = input.readUTF();
        int playerCount = input.readInt();
        if (serverName.equalsIgnoreCase("ALL")) {
          globalCount = playerCount;
        } else if (serverName.equalsIgnoreCase("UHC")) {
          uhcCount = playerCount;
        } else if (serverName.equalsIgnoreCase("minigames")) {
        	minigamesCount = playerCount;
        }
      }
    }
  }
  
  public void sendToBungeeCord(String channel, String sub) {
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    try {
      out.writeUTF(channel);
      out.writeUTF(sub);
    } catch (IOException e) {
      e.printStackTrace();
    }
    Bukkit.getServer().sendPluginMessage(Core.getInstance(), "BungeeCord", b.toByteArray());
  }
  public int getGlobalCount() { return globalCount; }
  public int getUHCCount() { return uhcCount; }
  public int getminigamesCount() { return minigamesCount; }
  public boolean isonlineminigames() { return isOnlineminigames; }
  public boolean isonlineUHC() { return isOnlineUHC; }
}
