package info.conturenetwork.ContureHUB.queue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.messaging.PluginMessageListener;

import info.conturenetwork.ContureHUB.Core;

public class BungeeMessage
  implements Listener, PluginMessageListener
{
  public Core plugin;
  
  public BungeeMessage(Core instance)
  {
    plugin = instance;
  }
  
  private QueueRank getRank(List<QueueRank> ranks, Player player) {
    QueueRank highestRank = null;
    for (QueueRank rank : ranks) {
      if (player.hasPermission(rank.getPermission())) {
        if (highestRank == null) {
          highestRank = rank;
        } else if (rank.getPriority() < highestRank.getPriority()) {
          highestRank = rank;
        }
      }
    }
    return highestRank;
  }
  
  public void onPluginMessageReceived(String channel, Player p, byte[] message)
  {
    DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
    try {
      String subchannel = in.readUTF();
      if (subchannel.equals("Rank")) {
        String input = in.readUTF();
        String[] args = input.split(",");
        Player player = Bukkit.getPlayer(UUID.fromString(args[0]));
        String queue = args[1];
        List<QueueRank> tempRanks = new ArrayList();
        if (player == null) {
          return;
        }
        for (int i = 2; i < args.length; i++) {
          tempRanks.add(new QueueRank(args[i].split(":")[0], Integer.valueOf(args[i].split(":")[1]).intValue()));
        }
        if (player.hasPermission("queue.bypass")) {
          sendRankToBungee(player, queue, null, true);
          return;
        }
        QueueRank rank = getRank(tempRanks, player);
        sendRankToBungee(player, queue, rank, false);
      }
      if (subchannel.equals("InvalidQueue")) {
        String input = in.readUTF();
        String[] args = input.split("~");
        Player player = Bukkit.getPlayer(UUID.fromString(args[0]));
        String invalidMessage = args[1];
        if (player != null) {
          player.sendMessage(invalidMessage);
        }
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private void sendRankToBungee(Player p, String queue, QueueRank rank, boolean bypass) {
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    try {
      out.writeUTF("ReturnRank");
      if (!bypass) {
        out.writeUTF(String.valueOf(String.valueOf(String.valueOf(p.getUniqueId().toString()))) + "," + queue + "," + (rank != null ? rank.getPermission() : "null"));
      } else {
        out.writeUTF(String.valueOf(String.valueOf(String.valueOf(p.getUniqueId().toString()))) + "," + queue + "," + "bypassqueue");
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    p.sendPluginMessage(Core.getInstance(), "ReturnRank", b.toByteArray());
  }
}
