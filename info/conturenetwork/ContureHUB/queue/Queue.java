package info.conturenetwork.ContureHUB.queue;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import info.conturenetwork.ContureHUB.Core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;



public class Queue
{
  private String server;
  private List<Player> players;
  private Map<Player, BukkitTask> taskMap;
  private boolean paused;
  private int limit;
  
  public Queue(String server)
  {
    this.server = server;
    players = new ArrayList();
    taskMap = new HashMap();
    paused = false;
    limit = 500;
    new BukkitRunnable() {
      public void run() {
        for (Player o : players)
          if (o.isOnline()) {
            Player p = o;
            p.sendMessage("§7§m-*----------------------------------*-");
            p.sendMessage("§dIf you want to skip the queue, purchase");
            p.sendMessage("§da rank at buycraft.§7(Soon)");
            p.sendMessage("§7§m-*----------------------------------*-");
          } else { players.remove(o);
          }
      }
    }.runTaskTimer(Core.getInstance(), 10L, 10L);
  }
  
  public void clear() {
    players.clear();
    taskMap.clear();
  }
  
  public void addEntry(Player p) {
    if (players.contains(p)) {
      return;
    }
    if (QueueManager.getInstance().getPriority(p) == 0) {
      p.sendMessage("§dYou bypassed queue for §f" + server + "§b.");
      connectServer(p.getName(), server);
      return;
    }
    p.sendMessage("§dYou have been added to queue for §f" + server + "§b.");
    players.add(p);
    for (Player u : players) {
      int pos = players.indexOf(u);
      if ((u != p) && (QueueManager.getInstance().getPriority(p) < QueueManager.getInstance().getPriority(u))) {
        if (((Player)players.get(pos)).isOnline()) {
          ((Player)players.get(pos)).sendMessage("§dSomeone with higher queue priority has joined the queue!");
        }
        Collections.swap(players, pos, players.size() - 1);
      }
    }
  }
  
  public void removeEntry(Player p) {
    if (!players.contains(p)) {
      return;
    }
    players.remove(p);
  }
  
  public int getSize() {
    return players.size();
  }
  
  public Player getPlayerAt(int p) {
    return (Player)players.get(p);
  }
  
  public void sendFirst() {
    if (!players.isEmpty()) {
      Player p = (Player)players.get(0);
      ByteArrayDataOutput out = ByteStreams.newDataOutput();
      out.writeUTF("Connect");
      out.writeUTF(server);
      p.sendPluginMessage(Core.getInstance(), "BungeeCord", out.toByteArray());
    }
  }
  
  public void sendDirect(Player p) {
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeUTF("Connect");
    out.writeUTF(server);
    p.sendPluginMessage(Core.getInstance(), "BungeeCord", out.toByteArray());
  }
  
  public void connectServer(String playerName, String server) {
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeUTF("ConnectOther");
    out.writeUTF(playerName);
    out.writeUTF(server);
    sendBungeeMessage(out);
  }
  
  public static void sendBungeeMessage(ByteArrayDataOutput message) {
    sendBungeeMessage(getFirstPlayer(), message);
  }
  
  public static void sendBungeeMessage(Player player, ByteArrayDataOutput message) {
    player.sendPluginMessage(Core.getInstance(), "BungeeCord", message.toByteArray());
  }
  
  @SuppressWarnings("deprecation")
public static Player getFirstPlayer() {
    return (Player)Iterables.getFirst(Arrays.asList(Bukkit.getOnlinePlayers()), null);
  }
  
  public String getServer() { return server; }
  
  public List<Player> getPlayers()
  {
    return players;
  }
  
  public Map<Player, BukkitTask> getTaskMap() {
    return taskMap;
  }
  
  public boolean isPaused() {
    return paused;
  }
  
  public void setPaused(boolean p) {
    paused = p;
  }
  
  public int getLimit() {
    return limit;
  }
  
  public void setLimit(int i) {
    limit = i;
  }
}
