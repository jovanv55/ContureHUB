package info.conturenetwork.ContureHUB.queue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitTask;

import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.utilities.Utils;

public class QueueManager implements Listener
{
  private List<Queue> queues;
  public static QueueManager instance;
  
  public QueueManager()
  {
    queues = new ArrayList();
    instance = this;
    Bukkit.getPluginManager().registerEvents(this, Core.getInstance());
    
    queues.add(new Queue("UHC"));
    queues.add(new Queue("UHCMeetup"));
    
    for (Queue q : queues) {
      Bukkit.getConsoleSender().sendMessage(
        Utils.colors("§7[§3ContureHub§7] &e" + q.getServer() + " je dodan za queue."));
    }
    








    new org.bukkit.scheduler.BukkitRunnable()
    {
      public void run()
      {
        for (Queue q : queues) {
          if ((!q.isPaused()) && (!q.getPlayers().isEmpty())) {
            q.sendFirst();
            q.removeEntry(q.getPlayerAt(0));
          }
        }
      }
    }.runTaskTimer(Core.getInstance(), 20L, 20L);
  }
  
  public void clear() {
    queues.clear();
  }
  
  public Queue getQueue(Player p) {
    for (Queue q : queues) {
      if (q.getPlayers().contains(p)) {
        return q;
      }
    }
    return null;
  }
  
  public Queue getQueue(String s) {
    for (Queue q : queues) {
      if (q.getServer().equalsIgnoreCase(s)) {
        return q;
      }
    }
    return null;
  }
  
  public String getQueueName(Player p) {
    return getQueue(p).getServer();
  }
  
  public int getPriority(Player player) {
    if (player.hasPermission("queue.bypass")) {
      return 0;
    }
    if (player.hasPermission("queue.1")) {
      return 1;
    }
    if (player.hasPermission("queue.2")) {
      return 2;
    }
    if (player.hasPermission("queue.3")) {
      return 3;
    }
    if (player.hasPermission("queue.4")) {
      return 4;
    }
    if (player.hasPermission("queue.5")) {
      return 5;
    }
    return 6;
  }
  
  @EventHandler
  public void onQuit(PlayerQuitEvent e) {
    Player p = e.getPlayer();
    for (Queue q : queues) {
      if (q.getPlayers().contains(p)) {
        q.removeEntry(p);
      }
      if (q.getTaskMap().containsKey(p)) {
        ((BukkitTask)q.getTaskMap().get(p)).cancel();
        q.getTaskMap().remove(p);
      }
    }
  }
  
  public List<Queue> getQueues() {
    return queues;
  }
  
  public static QueueManager getInstance() {
    return instance;
  }
}
