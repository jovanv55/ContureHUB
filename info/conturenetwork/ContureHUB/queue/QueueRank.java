package info.conturenetwork.ContureHUB.queue;

public class QueueRank
{
  private String permission;
  private int priority;
  
  public QueueRank(String permission, int priority) {
    this.permission = permission;
    this.priority = priority;
  }
  
  public String getPermission() {
    return permission;
  }
  
  public int getPriority() {
    return priority;
  }
}
