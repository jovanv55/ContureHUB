package info.conturenetwork.ContureHUB.utilities;

import info.conturenetwork.ContureHUB.Core;

public class Handler { 
  private Core plugin;
  
  public void enable() {}
  
  public void disable() {}
  
  public Handler(Core plugin) { this.plugin = plugin; }
  public Core getInstance() { return plugin; }
}
