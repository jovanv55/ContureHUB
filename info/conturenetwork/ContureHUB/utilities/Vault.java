package info.conturenetwork.ContureHUB.utilities;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicesManager;

import info.conturenetwork.ContureHUB.Core;

public class Vault
{
  public static Permission permission;
  public static Chat chat;
  
  public Vault() {}
  
  public static boolean hook()
  {
    RegisteredServiceProvider<Permission> permissionProvider = Core.getInstance().getServer().getServicesManager().getRegistration(Permission.class);
    RegisteredServiceProvider<Chat> chaat = Core.getInstance().getServer().getServicesManager().getRegistration(Chat.class);
    if (permissionProvider != null) {
      permission = (Permission)permissionProvider.getProvider();
      chat = (Chat)chaat.getProvider();
      org.bukkit.Bukkit.getConsoleSender().sendMessage("§7[§3ContureHub§7] §eVault uspesno povezan.");
    }
    return permission != null;
  }
  
  public static String getPlayerGroup(Player player) {
    return permission.getPrimaryGroup(player);
  }
  
  public static String getPlayerPrefix(Player player) {
    return chat.getPlayerPrefix(player).replace("&", "§");
  }
}
