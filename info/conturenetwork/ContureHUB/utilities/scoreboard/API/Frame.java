package info.conturenetwork.ContureHUB.utilities.scoreboard.API;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class Frame {
   private static Frame instance;
   private JavaPlugin plugin;
   private FrameAdapter adapter;
   private Map<UUID, FrameBoard> boards;

   public Frame(JavaPlugin plugin, FrameAdapter adapter) {
      if (instance != null) {
         throw new RuntimeException("Frame has already been instantiated!");
      } else {
         instance = this;
         this.plugin = plugin;
         this.adapter = adapter;
         this.boards = new HashMap();
         this.setup();
      }
   }

@SuppressWarnings("deprecation")
private void setup() {
      this.plugin.getServer().getPluginManager().registerEvents(new FrameListener(), this.plugin);
      this.plugin.getServer().getScheduler().runTaskTimerAsynchronously(this.plugin, () -> {
          Player[] onlinePlayers;
          int length = (onlinePlayers = Bukkit.getOnlinePlayers()).length;
          for (int k = 0; k < length; k++)
          {
            Player player = onlinePlayers[k];
            FrameBoard board = (FrameBoard)this.boards.get(player.getUniqueId());
            if (board != null) {
               Scoreboard scoreboard = board.getScoreboard();
               Objective objective = board.getObjective();
               if (!objective.getDisplayName().equals(this.adapter.getTitle(player))) {
                  objective.setDisplayName(this.adapter.getTitle(player));
               }

               List<String> newLines = this.adapter.getLines(player);
               if (newLines != null && !newLines.isEmpty()) {
                  Collections.reverse(newLines);
                  int i;
                  FrameBoardEntry entry;
                  if (board.getEntries().size() > newLines.size()) {
                     for(i = newLines.size(); i < board.getEntries().size(); ++i) {
                        entry = board.getEntryAtPosition(i);
                        if (entry != null) {
                           entry.remove();
                        }
                     }
                  }

                  for(i = 0; i < newLines.size(); ++i) {
                     entry = board.getEntryAtPosition(i);
                     String line = ChatColor.translateAlternateColorCodes('&', (String)newLines.get(i));
                     if (entry == null) {
                        entry = new FrameBoardEntry(board, line);
                     }

                     entry.setText(line);
                     entry.setup();
                     entry.send(i);
                  }
               } else {
                  board.getEntries().forEach(FrameBoardEntry::remove);
                  board.getEntries().clear();
               }

               player.setScoreboard(scoreboard);
            }
         }

      }, 20L, 2L);
   }

   public static Frame getInstance() {
      return instance;
   }

   public FrameAdapter getAdapter() {
      return this.adapter;
   }

   public Map<UUID, FrameBoard> getBoards() {
      return this.boards;
   }
}