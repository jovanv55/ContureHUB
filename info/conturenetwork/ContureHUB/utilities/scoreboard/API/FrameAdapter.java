package info.conturenetwork.ContureHUB.utilities.scoreboard.API;

import java.util.List;
import org.bukkit.entity.Player;

public abstract interface FrameAdapter
{
  public abstract String getTitle(Player paramPlayer);
  
  public abstract List<String> getLines(Player paramPlayer);
}
