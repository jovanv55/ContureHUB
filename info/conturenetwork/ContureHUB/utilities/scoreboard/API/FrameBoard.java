package info.conturenetwork.ContureHUB.utilities.scoreboard.API;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;




public class FrameBoard
{
  private final List<FrameBoardEntry> entries;
  private final List<String> identifiers;
  private Scoreboard scoreboard;
  private Objective objective;
  
  public FrameBoard(Player player)
  {
    entries = new ArrayList();
    identifiers = new ArrayList();
    
    setup(player);
  }
  
  private void setup(Player player)
  {
    if (player.getScoreboard().equals(Bukkit.getScoreboardManager().getMainScoreboard())) {
      scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    } else {
      scoreboard = player.getScoreboard();
    }
    

    objective = scoreboard.registerNewObjective("Default", "dummy");
    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    objective.setDisplayName(Frame.getInstance().getAdapter().getTitle(player));
    

    player.setScoreboard(scoreboard);
  }
  
  public FrameBoardEntry getEntryAtPosition(int pos) {
    if (pos >= entries.size()) {
      return null;
    }
    return (FrameBoardEntry)entries.get(pos);
  }
  
  public String getUniqueIdentifier(String text)
  {
    String identifier = getRandomChatColor() + ChatColor.WHITE;
    
    while (identifiers.contains(identifier)) {
      identifier = identifier + getRandomChatColor() + ChatColor.WHITE;
    }
    

    if (identifier.length() > 16) {
      return getUniqueIdentifier(text);
    }
    

    identifiers.add(identifier);
    
    return identifier;
  }
  
  private static String getRandomChatColor()
  {
    return ChatColor.values()[java.util.concurrent.ThreadLocalRandom.current().nextInt(ChatColor.values().length)].toString();
  }
  
  public Scoreboard getScoreboard() {
    return scoreboard;
  }
  
  public List<FrameBoardEntry> getEntries() {
    return entries;
  }
  
  public Objective getObjective() {
    return objective;
  }
  
  public List<String> getIdentifiers() {
    return identifiers;
  }
}
