package info.conturenetwork.ContureHUB.utilities.scoreboard.API;

import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class FrameBoardEntry
{
  private final FrameBoard board;
  private String text;
  private String identifier;
  private Team team;
  
  public FrameBoardEntry(FrameBoard board, String text)
  {
    this.board = board;
    this.text = text;
    identifier = this.board.getUniqueIdentifier(text);
    
    setup();
  }
  
  public void setup() {
    Scoreboard scoreboard = board.getScoreboard();
    String teamName = identifier;
    

    if (teamName.length() > 16) {
      teamName = teamName.substring(0, 16);
    }
    
    Team team = scoreboard.getTeam(teamName);
    

    if (team == null) {
      team = scoreboard.registerNewTeam(teamName);
    }
    

    if (!team.getEntries().contains(identifier)) {
      team.addEntry(identifier);
    }
    

    if (!board.getEntries().contains(this)) {
      board.getEntries().add(this);
    }
    
    this.team = team;
  }
  
  public void send(int position) {
    if (text.length() > 16) {
      String prefix = text.substring(0, 16);
      String suffix;
      if (prefix.charAt(15) == '§') {
        prefix = prefix.substring(0, 15);
        suffix = text.substring(15, text.length()); } else { suffix = "";
        if (prefix.charAt(14) == '§') {
          prefix = prefix.substring(0, 14);
          suffix = text.substring(14, text.length());
        } else { suffix = "";
          if (ChatColor.getLastColors(prefix).equalsIgnoreCase(ChatColor.getLastColors(identifier))) {
            suffix = text.substring(16, text.length());
          } else {
            suffix = ChatColor.getLastColors(prefix) + text.substring(16, text.length());
          }
        }
      }
      if (suffix.length() > 16) {
        suffix = suffix.substring(0, 16);
      }
      
      team.setPrefix(prefix);
      team.setSuffix(suffix);
    } else {
      team.setPrefix(text);
      team.setSuffix("");
    }
    
    org.bukkit.scoreboard.Score score = board.getObjective().getScore(identifier);
    score.setScore(position);
  }
  
  public void remove() {
    board.getIdentifiers().remove(identifier);
    board.getScoreboard().resetScores(identifier);
  }
  
  public void setText(String line) {
    text = line;
  }
}
