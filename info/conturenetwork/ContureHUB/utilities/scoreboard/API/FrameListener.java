package info.conturenetwork.ContureHUB.utilities.scoreboard.API;

import org.bukkit.event.player.PlayerJoinEvent;

public class FrameListener implements org.bukkit.event.Listener
{
  public FrameListener() {}
  
  @org.bukkit.event.EventHandler
  public void onPlayerJoin(PlayerJoinEvent event)
  {
    Frame.getInstance().getBoards().put(event.getPlayer().getUniqueId(), new FrameBoard(event.getPlayer()));
  }
  
  @org.bukkit.event.EventHandler
  public void onPlayerQuit(org.bukkit.event.player.PlayerQuitEvent event) {
    Frame.getInstance().getBoards().remove(event.getPlayer().getUniqueId());
  }
}
