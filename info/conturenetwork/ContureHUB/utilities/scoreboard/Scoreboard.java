package info.conturenetwork.ContureHUB.utilities.scoreboard;

import fr.xephi.authme.AuthMe;
import fr.xephi.authme.api.NewAPI;
import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.handlers.ServersHandler;
import info.conturenetwork.ContureHUB.queue.Queue;
import info.conturenetwork.ContureHUB.queue.QueueManager;
import info.conturenetwork.ContureHUB.utilities.Vault;
import info.conturenetwork.ContureHUB.utilities.scoreboard.API.FrameAdapter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings("deprecation")
public class Scoreboard implements FrameAdapter, Listener
{
  public Scoreboard(JavaPlugin plugin)
  {
    org.bukkit.Bukkit.getPluginManager().registerEvents(this, plugin);
  }
  
  public String getTitle(Player player) {
    String crta = org.apache.commons.lang.StringEscapeUtils.unescapeJava("┃");
    return "§d§lConture §7<crta> §fHub".replace("<crta>", crta);
  }
  
  public List<String> getLines(Player player)
  {
    List<String> board = new ArrayList();
    board.add("&7&m--------------------");
    if (AuthMe.getApi().isAuthenticated(player)) {
      int amount = Core.getInstance().getServersHandler().getGlobalCount();
      if (amount == 0) amount = 1;
    //  board.add("");
      board.add("&d§lYour Rank:");
      board.add(formatedrank(Vault.getPlayerGroup(player)));
      board.add("");
      board.add("&d§lOnline: ");
      board.add("<count>".replace("<count>", "" + amount));
      board.add("");
      if (QueueManager.getInstance().getQueue(player) != null) {
        String qpos = String.valueOf(QueueManager.getInstance().getQueue(player).getPlayers().indexOf(player) + 1);
        String qmax = String.valueOf(QueueManager.getInstance().getQueue(player).getSize());
        String qserver = QueueManager.getInstance().getQueueName(player).toUpperCase();
        board.add("&d§lQueued for: &f" + qserver);
        board.add("&d§lPosition: &f#" + qpos + " of &d#" + qmax);
        board.add("&f&d");
      }
    } else if (!AuthMe.getApi().isRegistered(player.getName())) {
      board.add("&d§lYou're not registered !");
      board.add("&d§lRegister with");
      board.add("&f/register <pass> <pass>");
      board.add("&d§lIf you have problem");
      board.add("&d§lGo to our Discord!");
      board.add("");
    } else {
      board.add("&d§lPlease login with the");
      board.add("&f/login <password>");
      board.add("&d§lOr you will be kicked!");
      board.add("");
    }
    board.add("&dmc.conturenetwork.tk");
    board.add("&7&m--------------------");
    board.add("");
    if (board.size() == 2) {
      return null;
    }
    return board;
  }
  private String formatedrank(String g) {
	  if (g.equalsIgnoreCase("Default")) {
		  return "§aDefault";
	  }
	  if (g.equalsIgnoreCase("Owner")) {
		  return "§cOwner";
	  }
	  if (g.equalsIgnoreCase("Developer")) {
		  return "§cDeveloper";
	  }
	  return "§aDefault";
  }
}
