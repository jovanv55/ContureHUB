package info.conturenetwork.ContureHUB.utilities.tab.API;

import org.bukkit.entity.Player;

public interface TabAdapter {
    TabTemplate getTemplate(Player player);
}