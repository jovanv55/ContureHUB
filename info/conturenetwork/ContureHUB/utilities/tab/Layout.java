package info.conturenetwork.ContureHUB.utilities.tab;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import info.conturenetwork.ContureHUB.Core;
import info.conturenetwork.ContureHUB.queue.QueueManager;
import info.conturenetwork.ContureHUB.utilities.Vault;
import info.conturenetwork.ContureHUB.utilities.tab.API.TabAdapter;
import info.conturenetwork.ContureHUB.utilities.tab.API.TabTemplate;

public class Layout implements Listener, TabAdapter
	{
	public Layout(JavaPlugin plugin)
	{
		org.bukkit.Bukkit.getPluginManager().registerEvents(this, plugin);
		new TabList(plugin, this);
	}
    public TabTemplate getTemplate(Player player) {
        TabTemplate template = new TabTemplate();
    	String status;
    	if (Core.getInstance().getServersHandler().isonlineminigames()) status = "§aOnline";
    	else status = "§4Offline";
    	String statusuhc;
    	if (Core.getInstance().getServersHandler().isonlineUHC()) statusuhc = "§aOnline";
    	else statusuhc = "§4Offline";
        String crta = org.apache.commons.lang.StringEscapeUtils.unescapeJava("┃");
        template.middle(0, "§d§lConture §7<crta> §fNetwork".replace("<crta>", crta));
        template.middle(2, "§7Online §f" + Core.getInstance().getServersHandler().getGlobalCount() + "/ 2000");
        template.middle(4, "§d§lRank");
        template.middle(5, Vault.getPlayerGroup(player));
        template.middle(9, "Servers");
        template.left(4, "§d§lFacebook");
        template.left(5, "§f@ContureNetwork");
        template.left(11, "§d§lUHC");
        template.left(12, "§fStatus: " + statusuhc);
    	if (Core.getInstance().getServersHandler().isonlineUHC() == true)
    	{
        template.left(13, "§fPlayers: " + Core.getInstance().getServersHandler().getUHCCount());	
    	}
        template.right(9, "§d§lSkyPvP");
        template.right(10, "§fStatus: " + status);
        if (Core.getInstance().getServersHandler().isonlineminigames()) 
        {
        template.right(11, "§fPlayers: " + Core.getInstance().getServersHandler().getminigamesCount());
        }
        template.right(4, "§d§lQueued for:");
        if (QueueManager.getInstance().getQueue(player) != null) {
        String qpos = String.valueOf(QueueManager.getInstance().getQueue(player).getPlayers().indexOf(player) + 1);
        String qmax = String.valueOf(QueueManager.getInstance().getQueue(player).getSize());
        String qserver = QueueManager.getInstance().getQueueName(player).toUpperCase();
        template.right(5, "§7" + qserver);
        template.right(6, "§d§lPosition:");
        template.right(7, "§f#" + qpos + " of &d#" + qmax);
        } else {
        template.right(5, "§fNone");  
        }
        return template;
    }
}